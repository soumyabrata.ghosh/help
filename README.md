# Help Wiki 

> <sup> for projects managed by Soumya </sup> 

#### Menu:
* [Wiki](https://git-r3lab.uni.lu/soumyabrata.ghosh/help/-/wikis/home)
  * [Wiki for Biomap](https://git-r3lab.uni.lu/soumyabrata.ghosh/help/-/wikis/biomap)


#### Help/Support Wiki for Projects.

- This project has public view access. Do not share any sensitive data here.
- Go to Wiki from the left menu (online).


#### Contact
Soumyabrata Ghosh / soumyabrata.ghosh@uni.lu